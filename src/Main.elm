module Main exposing (..)

import Actor
import Html exposing (Html, input, text, div, img)
import Html.Events exposing (onInput)
import Html.Attributes exposing (src, type_, placeholder, class, id, style)
import Time
import Tuple exposing (first, second)
import List.Extra as LE
import Random.Pcg as Rng




---- MODEL ----

type alias Model =
    { seed : Rng.Seed
    , controls : Controls
    , world : World
    }


asWorldIn : Model -> World -> Model
asWorldIn model world =
    { model | world = world }


asControlsIn : Model -> Controls -> Model
asControlsIn model controls =
    { model | controls = controls }


-- Controls for the simulation


type alias Controls =
    { seconds_per_tick : Float
    }

asSecondsPerTickIn : Controls -> Float -> Controls
asSecondsPerTickIn controls seconds_per_tick =
    { controls | seconds_per_tick = seconds_per_tick }


-- The simulated world


type alias World =
    { -- Number of ticks since simulation started
      num_ticks : Int

    , width : Int
    , height : Int

    -- All actors in the world
    , actors : List Actor.Actor
    -- Actions generated this tick
    , actions : List Actor.Action
    }


asNumTicksIn : World -> Int -> World
asNumTicksIn world num_ticks =
    { world | num_ticks = num_ticks }

asActorsIn: World -> List Actor.Actor -> World
asActorsIn world actors =
    { world | actors = actors }

asActionsIn: World -> List Actor.Action -> World
asActionsIn world actions =
    { world | actions = actions }

-- Random-length list of values produced by the generator parameter
randomList : Rng.Generator Int -> Rng.Generator a -> Rng.Generator (List a)
randomList size_generator elt_generator =
    size_generator
        |> Rng.andThen (\list_size -> Rng.list list_size elt_generator)


-- Generate an empty list
emptyListGenerator : Rng.Generator (List a)
emptyListGenerator =
    Rng.map (\i -> []) (Rng.int 0 1)


listOfGeneratorToGeneratorOfList : List (Rng.Generator a) -> Rng.Generator (List a)
listOfGeneratorToGeneratorOfList listOfGen =
    case listOfGen of
        [] ->
            emptyListGenerator
        gen :: rest ->
            Rng.map2 (::) gen (listOfGeneratorToGeneratorOfList rest)


-- Generate a random length list of actors for birth_tick spawned by
-- action_id
randomActorList : Rng.Generator Int -> Int -> Actor.ActionId ->
                  Rng.Generator Actor.ActorKind ->
                  Rng.Generator Actor.Position ->
                  Rng.Generator (List Actor.Actor)
randomActorList size_generator birth_tick action_id kind_gen pos_gen =
    let
        size_to_indices = \a size -> (List.range 1 size)
        index_to_id = Actor.actorIdFor birth_tick action_id
        id_to_actor_gen =
            \id ->  Actor.randomActor birth_tick id kind_gen pos_gen
        size_to_actor_gens =
            \size ->
                List.range 1 size
                  |> List.map (index_to_id >> id_to_actor_gen)
    in
        size_generator
            |> Rng.andThen (size_to_actor_gens >> listOfGeneratorToGeneratorOfList)


-- Generator producing a random world in its initial state (time 0)
--
-- Generator a is defined to be: Generator (Seed -> ( a, Seed ))
randomWorld : Rng.Generator World
randomWorld =
    let
        (w,h) = (15,7)
        num_actors = Rng.int 2 (w * h)
        kind_gen = Actor.randomKind 0.8
        pos_gen = Actor.randomPosition w h
        actors = randomActorList
                 num_actors 0 (Actor.actionIdFor -1 0 ) kind_gen pos_gen

        world_construct = \actors ->
            { num_ticks = 0
            -- Number of grid squares across
            , width = w
            -- Number of grid squares vertically
            , height = h
            , actors = actors
            , actions =
                []
            }
    in
        Rng.map world_construct actors


init : Flags -> ( Model, Cmd Msg )
init flags =
    let
        seed0 = Rng.initialSeed flags.randomSeed
        (world, seed1) = Rng.step randomWorld seed0
    in
        ( { seed = seed1
          , controls =
                { seconds_per_tick = 0.5
                }
          , world = world
          }
        , Cmd.none
        )



---- UPDATE ----


type Msg
    = Tick Time.Time
    | SecondsPerTick String

incNumTicks : Model -> Model
incNumTicks model =
    model.world.num_ticks + 1
        |> asNumTicksIn model.world
        |> asWorldIn model

applyActions : List Actor.Action -> Model -> Model
applyActions actions model =
    List.foldl applyAction model <| removeConflictingActions actions


moveActionConflictsWith : Actor.ActorId -> Actor.Position -> Actor.Action -> Bool
moveActionConflictsWith id pos b_action =
    case b_action of
        Actor.Move b_id b_pos ->
            (id == b_id && pos /= b_pos) -- same object moved to different places
            || (id /= b_id && pos == b_pos) -- different objects in same place
        Actor.DoNothing ->
            False

conflictingPair : (Actor.Action, Actor.Action) -> Bool
conflictingPair (a, b) =
    case a of
        Actor.Move id pos ->
            moveActionConflictsWith id pos b
        Actor.DoNothing ->
            False

nonConflicting : (Actor.Action, List Actor.Action) -> Maybe Actor.Action
nonConflicting (action, actions) =
    let
        pairs =
            List.map (\elt->(action, elt)) actions
    in
        if List.any conflictingPair pairs then
            Nothing
        else
            Just action

-- List of all the elements, each paired with the rest of the elements
-- in the list
--
-- Example: eltsWithRest [1,3,2] == [(1,[3,2]),(3,[1,2]),(2,[1,3])]
eltsWithRest : List a -> List (a, List a)
eltsWithRest raw_list =
    let
        split_at =
            (flip LE.splitAt) raw_list
        positions_making_left_side_non_empty =
            List.range 1 (List.length raw_list)
        split_off_last_elt_of_first (a,b) =
            (LE.splitAt (List.length a - 1) a,b)
        combine_first_and_last ((a,b),c) =
            (b, List.append a c)
        grab_first_of_first (a,b) =
            case List.head a of
                Just val ->
                    Just (val, b)
                Nothing ->
                    Nothing
    in
        List.filterMap grab_first_of_first
        <| List.map combine_first_and_last
        <| List.map split_off_last_elt_of_first
        <| List.map split_at positions_making_left_side_non_empty



removeConflictingActions : List Actor.Action -> List Actor.Action
removeConflictingActions actions =
    if List.length actions < 2 then
        actions
    else
        List.filterMap nonConflicting (eltsWithRest actions)

applyAction : Actor.Action -> Model -> Model
applyAction action model =
    case action of
        Actor.Move actor_id pos ->
            model.world.actors
                |> LE.updateIf (Actor.hasId actor_id) (Actor.asPosIn pos)
                |> asActorsIn model.world
                |> asWorldIn model
        Actor.DoNothing ->
            model

generateActions : Model -> List Actor.Action
generateActions model =
    -- TODO These are dummy actions
    let
        world = model.world
        width = world.width
        height = world.height
        actors = world.actors
        act actor =
            let
                (x,y) = actor.pos
            in
                case actor.kind of
                    Actor.Dragon info ->
                        Actor.Move actor.id ((x + 1) % width, (y + 1) % height)
                    Actor.Turkey info ->
                        if world.num_ticks % width == 0 then
                            Actor.DoNothing -- Ensure eventual collision
                        else
                            Actor.Move actor.id ((x + 1) % width, y)
    in
        List.map act actors

generateAndApplyActions : Model -> Model
generateAndApplyActions model =
    let
        actions =
            generateActions model
        applied =
            applyActions actions model
    in
        actions
        |> asActionsIn applied.world
        |> asWorldIn applied

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick newTime ->
            ( model
            |> incNumTicks
            |> generateAndApplyActions
            , Cmd.none
            )
        SecondsPerTick timeString ->
            case String.toFloat timeString of
                Err msg ->
                    (model, Cmd.none)
                Ok newTime ->
                    ( newTime
                    |> asSecondsPerTickIn model.controls
                    |> asControlsIn model
                    , Cmd.none
                    )


-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every (model.controls.seconds_per_tick * Time.second) Tick



---- VIEW ----


view : Model -> Html Msg
view model =
    div []
        [ div [] [ text "See the little dragons play!" ]
        , div []
            [ input [ type_ "text", placeholder "Seconds per tick", onInput SecondsPerTick ] []
            , text <| "Tick " ++ toString model.world.num_ticks
            ]
        , div -- Playfield
            [ style [ ( "position", "relative" ) ]
            ]
            (List.map (actorImage model.world.num_ticks) model.world.actors)
        ]


pix_per_image =
    60


type alias Positioned a =
    { a | pos : ( Int, Int ) }



-- CSS element-value pairs suitable for style atribute
-- Makes the element absolute position and offset by pix-per-image
-- Returns a list of CSS element-value pairs


gridPositionCss : Positioned a -> List ( String, String )
gridPositionCss positioned =
    [ ( "position", "absolute" )
    , ( "color", "green" )
    , ( "left", (toString <| (first positioned.pos) * pix_per_image) ++ "px" )
    , ( "top", (toString <| (second positioned.pos) * pix_per_image) ++ "px" )
    ]



-- Return the image HTML for a given actor

dragon_adult_age = 30

actorImageSrc : Int -> Actor.Actor -> String
actorImageSrc num_ticks_in_world actor =
  case actor.kind of
    Actor.Dragon info ->
      if num_ticks_in_world - actor.birth_tick >= dragon_adult_age then
        "img/adult_dragon.001.png"
      else
        "img/baby_dragon.001.png"
    Actor.Turkey info ->
      "img/turkey.001.png"

actorGridPositionCss : Actor.Actor -> List (String, String)
actorGridPositionCss actor =
    gridPositionCss actor

actorImage : Int -> Actor.Actor -> Html.Html msg
actorImage num_ticks_in_world actor =
  Html.img [
     src <| actorImageSrc num_ticks_in_world actor
     , style <| actorGridPositionCss actor ] []



---- FLAGS ----

type alias Flags =
    { randomSeed : Int
    }

---- PROGRAM ----


main : Program Flags Model Msg
main =
    Html.programWithFlags
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }
